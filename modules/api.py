import sys
import os
import tools
import time
# required for SSH and SFTP
import paramiko 
# required for UDP broadcast
import socket

# remote settings
# -----------------------------
# rpi default login
RPI_USERNAME = "pi"
RPI_PASSWORD = "raspberry"
CON_TIMEOUT = 5
# project path (repository name by default)
GIT_PATH = 'flowcapturecamera/'
# location of desired data on rpi
REMOTE_PATH = 'output/'
# captured files will be downloaded into this folder on local machines user folder
TARGET_PATH = r'output' 

# scripts on 
# remote: pi@raspberry~$GIT_PATH
# -----------------------------
SCRIPT_START_UDP = "UDPServer.py"
SCRIPT_START_TCP = "TCPServer.py"
SCRIPT_CLEAR_OUTPUT = "clear.py"
# deprecated (done by UDP broadcast)
SCRIPT_TAKE_IMAGE = "takePicture.py"
SCRIPT_TAKE_VIDEO = "takeVideo.py"

# UDP RPC triggers
# -----------------------------
UDP_TAKE_IMAGE = "image"
UDP_TAKE_VIDEO = "video"
UDP_CLEAR_OUTPUT = "clear"

# file transfer
# -----------------------------
TRANSFER_IMAGE = True
TRANSFER_VIDEO = False
# constants
# -----------------------------
MY_UDP_PORT = 5555

# runs a script on the remote hosts ip
# @param rpiScript string
# @param rpiScript an array of defined action can be found on top of this file
# @param ip string
# @param ip IP address of the remote host where the command should be executed
# @param awaitResponse boolean
# @param awaitResponse waits for the remote console log if true
#
# extendable:
# do multithreading for the RPC calls
#
def remoteScript(rpiScript, ip, awaitResponse):
    remoteCommand('sudo python ~/{git}{script}'.format(git=GIT_PATH, script=rpiScript), ip, awaitResponse)

# runs a custom console command on the remote hosts ip
# @param command string
# @param command a custom console command
# @param ip string
# @param ip IP address of the remote host where the command should be executed
# @param awaitResponse boolean
# @param awaitResponse waits for the remote console log if true
#
def remoteCommand(command, ip, awaitResponse):
    print "client: establish connection to remote host"
    print "remote ({0}): run {1}".format(ip,command)

    try:
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(ip, username=RPI_USERNAME, password=RPI_PASSWORD, timeout=CON_TIMEOUT)

        # fixed bug where UDP server crashes on remote, see ..
        # https://github.com/paramiko/paramiko/issues/189
        client.get_transport().set_keepalive(10)

        # run a command on the remote machine
        # note: SSHClient.exec_command() returns the tuple (stdin,stdout,stderr)
        # note: any paths in the remote script now relate to the user folder '~' not to the project folder
        tupel = client.exec_command(command)

        # monitor remote console output
        if awaitResponse:
            stdout = tupel[1]
            stderr = tupel[2]
            for line in stdout:
                print line
            for line in stderr:
                print line

        # cleanup
        client.close()
            
    except Exception,e:         
        print "remote ({0}): command {1} failed".format(ip, command)
        print str(e)

# initiates the capture process on the raspberries via UDP broadcast
# raspberries will listen to the broadcast and start video capturing
# @param UDPTask string
# @param UDPTask an array of defined action can be found on top of this file
#
# note: 
# requires remote UDPServer to listen on port MY_UDP_PORT (5555 by default)
#
def broadcastUDP(UDPtask):
    if not UDPtask:
        print "broadcastUDP(): no args, broadcasting 'hello' message instead.."
        return -1

    print "client: performing UDP broadcast on port {0}..".format(MY_UDP_PORT)

    udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    udp.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, True)
    udp.settimeout(5)

    # sendto(message:String, (ip,port):SocketObject)
    udp.sendto(UDPtask, ("<broadcast>", MY_UDP_PORT))

    try:
        print "{0}".format(udp.recv(1024))
    except Exception,e: 
        print "client: no UDP servers found"
        print str(e)

    udp.close()

# transfer files from rpis to client
# @param transferType BOOLEAN
# @param transferType True if the recorded image, false if the recorded video should be transfered from remote host
# @param remoteIP string
# @param remoteIP address of the host holding desired data
#
def transferFiles(transferType, remoteIP, pathPrefix):
    print "client: establish connection to remote host"

    # supporting windows and unix based OS for client
    localpath = tools.getHomeDirectory() 
    localpath += tools.getOSslash()

    # print "TEST: {0}".format(localpath + TARGET_PATH)

    # create outputfolder if not existent
    if not os.path.exists(localpath + TARGET_PATH):
        print "client: creating folder at {0}".format(localpath + TARGET_PATH)
        os.makedirs(localpath + TARGET_PATH)

    # define paths by capturing type
    # and add a timestamp to the filename
    # and add a capturecount for each capturing session
    timestr = time.strftime("%Y%m%d-%H%M") # append '%S' to format, if several captures per minute are taken
    remotepath = REMOTE_PATH
    remotepath += "capture.jpg" if transferType else "capture.h264"
    savepath = localpath + TARGET_PATH + tools.getOSslash()
    savepath += ( "TAKE{prefix}-{timestamp}-{ip}.jpg".format(prefix=pathPrefix,timestamp=timestr,ip=remoteIP) if transferType else "TAKE{prefix}-{timestamp}-{ip}.h264".format(prefix=pathPrefix,timestamp=timestr,ip=remoteIP) )

    # transfer data to the client machine
    try:
        # connect to remote hosts
        print "client: downloading data from remote host.."
        print "client: from {0}: {1}".format(remoteIP, remotepath)
        print "client: to local: {0}".format(savepath)

        # connect to client machine
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(remoteIP, username=RPI_USERNAME, password=RPI_PASSWORD, timeout=CON_TIMEOUT)

        # transfer data
        sftp = client.open_sftp()
        sftp.get(remotepath, savepath)
        sftp.close()
        client.close()

        print "remote ({0}): transfer successful".format(remoteIP)

    except Exception,e: 
        print "remote ({0}): filetransfer failed".format(remoteIP)
        print str(e)

#
# TODO 
# this will be replaced by the webserver again
#
if __name__ == '__main__':
    # print user information
    print "client OS: {0}".format(os.name)
    print "\n*********************************************"
    print "flow capture - single device testing"
    print "by dominik hoffendahl (d.hoffendahl@mytum.de)"
    print "*********************************************"

    # args
    remoteIP = ""
    command = 0

    # show menu
    if len(sys.argv) < 2:
        print "missing argument:"
        print "api.py <remote_ip>\n"
        sys.exit(0)

    # parse args
    remoteIP = sys.argv[1]

    # show menu
    mode = ""
    while (mode != 0):
        print "\n-------------------------------------------"
        print "MENU"
        print "-------------------------------------------"
        print "0 - EXIT"
        print "1 - Setup new device for capturing"
        print "2 - Perform UDP reliability test"
        print "3 - Perform TCP delay test (deprecated)"
        print "-------------------------------------------"
        print "CAPTURING"
        print "-------------------------------------------"
        print "11 - start UDP server on remote"
        print "12 - clear output folders on remote"
        print "21 - broadcast 'capture Video'"
        print "22 - transfer captured video from remote to client"
        print "31 - broadcast 'capture Picture'"
        print "32 - transfer captured image from remote to client"
        
        # read user input
        try:
            mode=int(raw_input('input: '))
        except ValueError:
            print "error"
            sys.exit(0)

        # handle menu selection
        if mode == 0:
            pass

        # SUBMENU
        elif mode == 1:
            exit = -1
            while (exit != 0):
                if exit == 0:
                    pass
                elif exit == 1:
                    print "client: clone git on remote"
                    remoteCommand(
                        "sudo git clone https://DHO1986:capture@bitbucket.org/DHO1986/bachelorarbeitrpi.git", 
                        remoteIP, 
                        True)
                elif exit == 2:
                    print "client: install packages on remote"
                    remoteCommand(
                        "sudo sh " + GIT_PATH + "package.sh", 
                        remoteIP,
                        True)
                else:
                    print "command {0} out of range".format(command)

                print "\n-------------------------------------------"
                print "MENU"
                print "-------------------------------------------"
                print "0 - BACK"
                print "1 - Clone git on remote machine"
                print "2 - Upgrade and install required packages"

                try:
                    exit=int(raw_input('input: '))
                except ValueError:
                    print "error"
                    sys.exit(0)
        
        # SUBMENU
        elif mode == 2:
            # make sure that UDP server is running on remote
            # print "client: start UDP server on remote"
            # remoteScript(
            #     SCRIPT_START_UDP,
            #     remoteIP,
            #     False)

            exit = -1
            while (exit != 0):
                if exit == 0:
                    pass
                elif exit == 1:
                    print "client: clear test, init new test"
                    broadcastUDP("testclear")
                elif exit == 2:
                    print "client: UDP TEST RUNNING"
                    print "client: sending 10000 packages in 50 seconds.."
                    for count in range(10000):
                        broadcastUDP("test")
                elif exit == 3:
                    print "client: fetching UDP test result.."
                    broadcastUDP("testresult")
                else:
                    print "command {0} out of range".format(command)

                print "\n-------------------------------------------"
                print "MENU"
                print "-------------------------------------------"
                print "0 - BACK"
                print "1 - Start new test"
                print "2 - Run UDP reliability test"
                print "3 - Show results"

                try:
                    exit=int(raw_input('input: '))
                except ValueError:
                    print "error"
                    sys.exit(0)

        # SUBMENU
        elif mode == 3:
            # make sure that TCP server is running on remote
            # print "client: start TCP server on remote"
            # remoteScript(
            #     SCRIPT_START_TCP,
            #     remoteIP,
            #     True)

            exit = -1
            while (exit != 0):
                if exit == 0:
                    pass
                # elif exit == 1:
                #     print "client: clear test, init new test"

                #     # fast inline script (dirty)
                #     # SINGLE DEVICE copy
                #     # documentary use only
                #     # multiple device test was already made in 12/2014
                #     import socket
                #     TCP_IP = remoteIP
                #     TCP_PORT = 80
                #     BUFFER_SIZE = 1024
                #     MESSAGE = "takeImage"

                #     # connect to server
                #     s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                #     s.connect((TCP_IP, TCP_PORT))

                #     for x in range(0, 100):
                #         s.send(MESSAGE)
                #         data = s.recv(BUFFER_SIZE)
                #         print "client: take image {0}".format(x)

                #     # cleanup
                #     s.close()
                else:
                    print "command {0} out of range".format(command)

                print "\n-------------------------------------------"
                print "DEPRECATED: not supported anymore"
                print "TCP syncronization test will capture 100 images" 
                print "on the remote machines"
                print "-------------------------------------------"
                print "0 - BACK"

                try:
                    exit=int(raw_input('input: '))
                except ValueError:
                    print "error"
                    sys.exit(0)

        # capturing methods..
        elif mode == 11:
            print "client: start UDP server on remote"
            remoteScript(
                SCRIPT_START_UDP,
                remoteIP,
                False)
        elif mode == 12:
            print "client: clear output folders on remote"
            remoteScript(
                SCRIPT_CLEAR_OUTPUT,
                remoteIP,
                True)

        elif mode == 21:
            print "client: broadcast 'capture Video'"
            broadcastUDP(UDP_TAKE_VIDEO)
        elif mode == 22:
            print "client: transfer captured video from remote to client"
            transferFiles(
                TRANSFER_VIDEO,
                remoteIP,
                "")

        elif mode == 31:
            print "client: broadcast 'capture Picutre'"
            broadcastUDP(UDP_TAKE_IMAGE)
        elif mode == 32:
            print "client: transfer captured image from remote to client"
            transferFiles(
                TRANSFER_IMAGE,
                remoteIP,
                "")
        else:
            print "command {0} out of range".format(command)



    

