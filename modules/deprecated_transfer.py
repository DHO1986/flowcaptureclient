'''''''''''''''''''''''''''''''''''''''''''''''''''
DEPRECATED
get data from remote host via sftp

can be driven in debug mode by adding arg '-debug'
'''''''''''''''''''''''''''''''''''''''''''''''''''

import sys
import os
import picam
import paramiko

# defaults
targetIP = "192.168.178.56"

# constants
RPI_USERNAME = "pi"
RPI_PASSWORD = "raspberry"
CON_TIMEOUT = 5
REMOTE_PATH = 'output/'
LOCAL_PATH_UNIX = 'output/'
LOCAL_PATH_NT = r"%HOMEPATH%\\flowcapture\\"

if __name__ == '__main__':
    
    # parse args, overwrite defaults
    if len(sys.argv) == 2:
        targetIP = sys.argv[1]
        clientOS = os.name
        # remotepath depending on client OS
        # supporting windows and unix based OS
        remotepath = REMOTE_PATH
        localpath = REMOTE_PATH_NT if clientOS == "nt" else REMOTE_PATH_UNIX
    elif len(sys.argv) == 2:
        # leave defaults as they are
        if sys.argv[1] == "-debug":
            pass
    else:
        print ">> error: missing args\n{input1}\n{input2}".format(
            input1="host - remote host carrying desired data"
        )
        sys.exit(0)

    # transfer data to the client machine
    try:
        print ">> downloading data from remote host.."
        print ">> FROM (host): {0}".format(remotepath)
        print ">> TO (client): {0}".format(localpath)
        
        # connect to client machine
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(targetIP, username=RPI_USERNAME, password=RPI_PASSWORD, timeout=CON_TIMEOUT)
   
        # transfer data
        sftp = client.open_sftp()
        sftp.get('output/*', remotepath)
        sftp.close()
        client.close()
        sys.exit(0)
        
    except IndexError:
        print ">> RPI: failed to send data"


