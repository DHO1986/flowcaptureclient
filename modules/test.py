# required for UDP broadcast
import socket
MY_UDP_PORT = 5555

def broadcastUDP(UDPtask):
    if not UDPtask:
        print "broadcastUDP(): no args, broadcasting 'hello' message instead.."
        return -1

    print "client: performing UDP broadcast on port {0}..".format(MY_UDP_PORT)

    udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    udp.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, True)
    udp.settimeout(5)

    # sendto(message:String, (ip,port):SocketObject)
    udp.sendto(UDPtask, ("<broadcast>", MY_UDP_PORT))

    try:
        print "{0}".format(udp.recv(1024))
    except Exception,e: 
        print "client: no UDP servers found"
        print str(e)

    udp.close()

if __name__ == "__main__":
	dictVid = {
		"task": "video",
		"length": 5000
	}
	
	str1 = str(dictVid)

	print "client: sending {0}".format(str1)
	
	broadcastUDP(str1)