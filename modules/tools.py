import os
from os.path import expanduser

# returns os dependant home directory
def getHomeDirectory():
	return expanduser("~")

# return os dependant dir seperators
def getOSslash():
	return "\\" if os.name == "nt" else "/"