# For more options and information see 
# http://www.raspberrypi.org/documentation/configuration/config-txt.md

# enable camera
start_x=1

# memory split settings (in MB)
gpu_mem=128

# sdtv_aspect=1  4:3
# sdtv_aspect=2  14:9
# sdtv_aspect=3  16:9