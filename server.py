import os, os.path
import random
import string
from modules import api as FlowCapture

import cherrypy

# notes:
# cherrypy session get and put
# > return cherrypy.session['mystring']
# > cherrypy.session['mystring'] = some_string

# user input
# -----------------------------
myIP = "192.168.178.57"
rpiIPs = [
    "131.159.40.167",
    "131.159.40.168",
    "131.159.40.170"
]
# increases for ever take of the current capturing session (like on any DLSR)
g_capture_count = 0 

# webapp
# -----------------------------
def errorJSON(message):
    print "client: error --> {0}".format(message)
    return {
        "message":"{0}".format(message),
        "flag":False
    }

def successJSON(message):
    print "client: success --> {0}".format(message)
    return {
        "message":"{0}".format(message),
        "flag":True
    }

def debugJSON(json):
    print "\nrequest params:"
    for key in json: 
            print "> {0} : {1}".format(key,cherrypy.request.params[key])
    print ""

# request tree
# -----------------------------
# provides landingpage and error message for the webapp
class App(object):
    @cherrypy.expose
    def index(self):
        return file('app/index.html')

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def default(self, attr='abc'):
        return errorJSON("No URI matches the given path")

# @URL: /api/udp
# @INP: JSON
# @OUT: JSON
class UDPHandler:
    exposed = True

    #
    # get UDP servers running on raspberryPis
    #
    @cherrypy.tools.json_out()
    def GET(self, **params):
        debugJSON(cherrypy.request.params)

        # start UDP server on the remote host
        for ip in rpiIPs:
            FlowCapture.remoteScript(FlowCapture.SCRIPT_START_UDP,ip,False)

        # output
        return successJSON("started UDP servers on raspberryPis.")

    #
    # post UDP message to trigger remote command
    #
    @cherrypy.tools.json_out()
    def POST(self, **params):
        debugJSON(cherrypy.request.params)

        # parse request
        message = cherrypy.request.params["message"]

        # broadcast message on UDP channel
        if message in ["image","video","clear"]:
            FlowCapture.broadcastUDP(message)
        else:
            return errorJSON("UDP message was {0}, but it has to be either: 'image','video','clear'".format(message))

        # output
        return successJSON("broadcasting UDP message: '{0}'".format(message)) 


# @URL: /api/transfer
# @INP: JSON
# @OUT: JSON
class TransferHandler:
    exposed = True

    #
    # get either image or video file from each rpi via sftp
    #
    @cherrypy.tools.json_out()
    def GET(self, **params):
        debugJSON(cherrypy.request.params)

        # parse request
        message = cherrypy.request.params["message"]

        # broadcast message on UDP channel
        if message not in ["image","video"]:
            return errorJSON("Transfer type was {0}, but it has to be either: 'image','video'".format(message))
        else:
            mode = FlowCapture.TRANSFER_IMAGE if message == "image" else FlowCapture.TRANSFER_VIDEO

        # transfer files from remote to client
        # client downloading files from remote
        for ip in rpiIPs:
           FlowCapture.transferFiles(mode,ip, g_capture_count) 
        global g_capture_count
        g_capture_count += 1

        # output
        return successJSON("successfully transfered files to client.")

    #
    # post clear outputfolders
    #
    @cherrypy.tools.json_out()
    def POST(self, **params):
        debugJSON(cherrypy.request.params)

        # start UDP server on the remote host
        for ip in rpiIPs:
            FlowCapture.remoteScript(FlowCapture.SCRIPT_CLEAR_OUTPUT,ip,True)

        # output
        return successJSON("outputfolders cleared.")


if __name__ == '__main__':
    # get remote hosts from known_hosts file
    with open("known_hosts") as f:
        global rpiIPs
        rpiIPs= [x.strip('\n') for x in f.readlines()]

    # api setup
    config = {
        '/': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'tools.sessions.on': True,
            'tools.response_headers.on': True,
            'tools.response_headers.headers': [('Content-Type', 'application/json')]
        }
    }

    # bind it to '/api/*'
    cherrypy.tree.mount(UDPHandler(), '/api/udp', config)
    cherrypy.tree.mount(TransferHandler(), '/api/transfer', config)

    # launch webapp
    appConfig = os.path.join(os.path.dirname(__file__),'config.cfg')
    cherrypy.quickstart(App(), '/', config=appConfig)


### TODO

# # SETUP NEW DEVICE 
# # TMP COPY
# print "clone git on remote"
# remoteCommand(
#     "sudo git clone https://DHO1986:capture@bitbucket.org/DHO1986/bachelorarbeitrpi.git", 
#     remoteIP, 
#     True)


# print "install packages on remote"
# remoteCommand(
#     "sudo sh " + GIT_PATH + "package.sh", 
#     remoteIP,
#     True)
